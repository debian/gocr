Source: gocr
Section: graphics
Priority: optional
Maintainer: Andrius Merkys <merkys@debian.org>
Build-Depends:
 bzip2 <!nocheck>,
 debhelper-compat (= 13),
 libjpeg-progs <!nocheck>,
 libnetpbm11-dev,
 netpbm <!nocheck>,
Standards-Version: 4.2.1
Homepage: https://www-e.uni-magdeburg.de/jschulen/ocr/
Vcs-Browser: https://salsa.debian.org/debian/gocr
Vcs-Git: https://salsa.debian.org/debian/gocr.git

Package: gocr
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 bzip2,
 libjpeg-progs,
 netpbm,
 transfig,
Description: Command line OCR
 This is a multi-platform OCR (Optical Character Recognition) program.
 .
 It can read pnm, pbm, pgm, ppm, some pcx and tga image files.
 .
 Currently the program should be able to handle well scans that have their text
 in one column and do not have tables. Font sizes of 20 to 60
 pixels are supported.
 .
 If you want to write your own OCR, libgocr is provided in a separate
 package. Documentation and graphical wrapper are provided in separated
 packages, too.

Package: gocr-tk
Architecture: all
Depends:
 gocr,
 tk | wish,
 xli,
 ${misc:Depends},
Suggests:
 xsane,
Description: tcl/tk wrapper around gocr
 This is a multi-platform OCR (Optical Character Recognition) program.
 .
 It can read pnm, pbm, pgm, ppm, some pcx and tga image files.
 .
 It is a tlc/tk GUI to gocr (a command line program).

Package: libpgm2asc-dev
Architecture: any
Section: libdevel
Depends:
 libpgm2asc0.52 (= ${binary:Version}),
 ${misc:Depends},
Provides:
 gocr-dev,
Conflicts:
 gocr-dev,
Replaces:
 gocr-dev,
Description: Command line OCR - development files
 This is a multi-platform OCR (Optical Character Recognition) program.
 .
 It can read pnm, pbm, pgm, ppm, some pcx and tga image files.
 .
 These are the development files.

Package: libpgm2asc0.52
Architecture: any
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: Command line OCR - shared library
 This is a multi-platform OCR (Optical Character Recognition) program.
 .
 It can read pnm, pbm, pgm, ppm, some pcx and tga image files.
 .
 This package contains the shared library.
